# InventoryDB_Internship2021

The file "Code_DBInventory_Week_18" is the main code that I did during my internship at SOPHIA Genetics in 2021.

The other file "TargetRefLesionName" is a file that contains a function that is used in the main code.

# Objectives 

The objective is to create an automatic inventory database thanks to data stored in a clinical database and thanks to two tables "models" and "pathology" (supposed to be in csv files but here in the main code we've created them).
Thanks to this data we create several tables "Series_inventory", "target_ref_series", "learning", "prediction", "processing" that we store in a new database "InventoryDB_vX". Knowing that "X" is the number of the version.



