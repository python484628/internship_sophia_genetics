#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 11:50:55 2021

@author: estoupmarion
"""

##################  LIBRARIES

#Import of libraries / modules
# sqlite3 for connection to databases
import sqlite3

# pandas as pd and numpy as np
import pandas as pd
import numpy as np

# datetime and timedelta for image removal
from datetime import datetime, timedelta

# os for versioning of DB (looking for files in the path)
import os

# re for looking for regex (strings)
import re

# json to export tables to a JSON format
import json


#### Import of created functions

# Function TargetRefLesionName to choose ref name that contains PredCorr first or From second or Pred third 
from TargetRefLesionName import *



################# VERSIONING OF INVENTORY_DB

# Path where we store InventoryDB
path = r'/Users/estoupmarion' 

# Creation of a variable previous_version (initialized at -1)
previous_version = -1
# For the available files in the path/directory
for filename in os.listdir(path): 
    # If the files start with InventoryDB and end with .db
    if (filename.startswith("InventoryDB") and filename.endswith(".db")): 
        # Then previous version takes the max value between value of previous version and value (X) of the latest version of InventoryDB_vX in the path
        previous_version = max(previous_version,int(re.search(r'\d+', filename).group(0)))

# Version is equal to previous_version + 1
version = (previous_version+1) 



################# CONNECTION TO DATABASES

# Connection to InventoryDB_vX with X being the value of version (previous_version+1)
# If no InventoryDB exist, it creates the first one with version = 0
conn_version = sqlite3.connect("InventoryDB_v{}.db".format(version))

        
# Connection to radiomics DB
conn_rad = sqlite3.connect('/Users/estoupmarion/Downloads/DB-inventory/DBlungT2.radiomics')
#conn_rad = sqlite3.connect('/Users/estoupmarion/Downloads/DB-inventory/DB-inventory.radiomics')
  

################# IMPORT OF TABLES FROM RADIOMICS DB FOR SERIES_INVENTORY TABLE
      
# Import of table patient stored in Patient_rad
Patient_rad = pd.read_sql_query("SELECT sophia_id, last_name as patient_ID FROM patient", conn_rad)

# Import of table study stored in Study_rad
Study_rad = pd.read_sql_query("SELECT id as study_id, patient as sophia_id, examination_date as study_date FROM study", conn_rad)

# Import of table series stored in Series_rad
# Last change : id as series_id_rad !
Series_rad = pd.read_sql_query("SELECT id as series_id_rad, study as study_id, modality, description as series_name, orientation as acquisition_axis, voxel_size_x as x_voxel_size, voxel_size_y as y_voxel_size, voxel_size_z as z_voxel_size, import_date, u_id as series_uid FROM series", conn_rad)
  

# Import of table pathology
# For now we create it but later probably a command to import a csv file or other format

pathology = {'patho_id': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                                 11, 12, 13, 14, 15],
    
    
               'pathology': ['nsclc','nsclc','nsclc','nsclc','nsclc','nsclc',
                                        'nsclc', 'glioma', 'meningioma', 'gbm', 'gbm',
                                        'gbm', 'gbm', 'kidney_tumor', 'kidney_tumor'],
               
               'target': ['lung', 'tum', 'liver', 'livermet', 'adrenalmet', 'bonemet',
                          'brainmet', 'tum', 'tum', 'tum', 'enhancement', 'necrosis',
                          'edema', 'kidney', 'tum'],
               
               'ref_loc': ['T', 'T', 'A', 'A', 'A', 'TAP', 'H', 'H', 'H', 'H', 'H', 'H',
                           'H', 'A', 'A'],
               
               'ref_axis': ['F\H', 'F\H', 'F\H', 'F\H', 'F\H', 'F\H', 'F\H',
                            'F\H', 'F\H', 'F\H', 'F\H', 'F\H', 'F\H', 'F\H',
                            'F\H'],
               
               'ref_modality': ['CT', 'CT', 'CT', 'CT', 'CT', 'CT', 'CT', 'MRI', 'MRI',
                                'MRI', 'MRI', 'MRI', 'MRI', 'CT', 'CT'],
               
               'ref_signal': ['Par', 'Par', 'TAP', 'TAP', 'TAP', '', 'Brain', 't2flair',
                              't1ce', 't1ce', 't1ce', 't1ce', 't2flair', 'portal', 'portal'],
               
               'backup_ref_signal': ['Fdg', 'Fdg', 'AP', 'AP', 'AP', '', '', '', 't1bravo',
                                     't1bravo', 't1bravo', 't1bravo', '', 'arterial', 'arterial'],
               
               
               'additional_modality': ['', 'CT', '', '', '', '', '', '', '', 'MRI',
                                       '', '', '', '', ''],
               
               'additional_signal': ['', 'Med', '', '', '', '', '', '', '', 't2flair',
                                     '', '', '', '', ''],
               
               'backup_additional_signal': ['', 'TAP', '', '', '', '', '', '', '', '',
                                            '', '', '', '', ''],
               
               'multimodal_ID_extraction': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}


# And we create pathology as a dataframe
pathology= pd.DataFrame(pathology)


################## CREATION OF SERIES_FROM_RAD TABLE FROM RADIOMICS DB
# We create the table series_from_rad by merging patient_rad and study_rad tables from radiomics DB
Series_from_rad = pd.merge(Patient_rad, Study_rad) 

# Then we merge it with series_rad 
Series_from_rad = pd.merge(Series_from_rad, Series_rad)

# Then we select the columns of Series_from_rad to keep because we have all columns of patient_rad, study_rad and series_rad
Series_from_rad = Series_from_rad[['patient_ID', 'series_id_rad','study_id',
                                   'study_date','modality', 'series_name','acquisition_axis',
                                   'x_voxel_size', 'y_voxel_size', 'z_voxel_size', 'import_date','series_uid']]



################### CREATION OF SERIES_INVENTORY TABLE IF INVENTORY_DB EXISTS OR NOT  

# If an Inventory DB exists (if version >0) we connect to it thanks to the variable previous_version that contains the value of the latest InventoryDB
# We store this connection in the variable conn_previous 
if version > 0:
    conn_previous =  sqlite3.connect("InventoryDB_v{}.db".format(previous_version))
    
    # We import manual columns from Series_inventory table from the latest InventoryDB
    # We store those columns in Series_from_prev table
    Series_from_prev = pd.read_sql_query("SELECT series_id as prev_series_id, series_uid, project, cohort, pathology, clinical_id, timepoint, signal FROM Series_inventory", conn_previous) 
    # We merge Series_from_prev with Series_from_rad to create Series_inventory table which is now filled with current data from radiomics DB thanks to Series_from_rad and filled with old manual data from the latest InventoryDB thanks to Series_from_prev
    # Both tables have series_uid in common
    Series_inventory = pd.merge(Series_from_rad, Series_from_prev, how='left') # left join to keep the current data coming from radiomics DB and to bring only the manual data from series_from_prev 
    
    
# If Inventory DB doesn't exist we create Series_table equals to Series_from_rad 
else:
    Series_inventory = Series_from_rad.copy()
    
    # We add the column prev_series_id and we put -1 inside
    Series_inventory['prev_series_id']= (-1)
    
    # And we add empty columns in the first series_table when there isn't any InventoryDB created yet because they are manual columns that will be filled in InventoryDB
    Series_inventory[['clinical_id', 'cohort', 'timepoint', 'pathology',
                  'signal', 'filter_type', 'project']]=''
    





###################### PROCESSING OF SERIES TABLE

# Processing of modality column
# We convert modality column into string to be able to change the codes
Series_inventory['modality'] = Series_inventory['modality'].apply(str) 
# We replace number 1 by CT, number 2 by MRI, number 3 by PT, number 4 by NM in modality column
Series_inventory['modality'] = Series_inventory['modality'].replace(['1','2','3','4'],['CT','MRI','PT','NM']) 

# Creation of image_removal column
# We create a variable date_difference that calculates the date difference between the current date and the dates in import date column
date_difference = pd.DataFrame((pd.to_datetime('now') - pd.to_datetime(Series_inventory['import_date'])) / np.timedelta64(1, 'Y')) 
# If import_date is superior than 5 years then we put 1 in the column image removal
Series_inventory.loc[date_difference['import_date'] >= 5, 'image_removal'] = 1 
# If import_date is inferior than 5 years then we put nothing in the column image removal
Series_inventory.loc[date_difference['import_date'] < 5, 'image_removal'] = ''

# If we want to remove series that have 1 in image_removal we can do this command
#Series_inventory = Series_inventory[Series_inventory.image_removal != 1]

    
# Creation of filter_type column
# Signal ParXX, Med, TAP, AP, Brain, FdgBody,TA, A become soft in filter_type
# Signal Par and Fdg become hard in filter_type
# If other signals (portal, t2flair, t1ce, we need to add condition otherwise filter_type is empty for those new signals)

# filter_type soft
Series_inventory.loc[Series_inventory['signal'].str.contains('([Par])\w+\d+', regex=True) | Series_inventory['signal'].str.contains('Med') | Series_inventory['signal'].str.contains('TAP') | Series_inventory['signal'].str.contains('AP') | Series_inventory['signal'].str.contains('Brain') | Series_inventory['signal'].str.contains('FdgBody') | Series_inventory['signal'].str.contains('TA') | Series_inventory['signal'].str.contains('A'), 'filter_type'] = 'soft'

# filter_type hard
Series_inventory.loc[Series_inventory['signal']=='Par','filter_type'] = 'hard'
Series_inventory.loc[Series_inventory['signal']=='Fdg','filter_type'] = 'hard'




# Creation of localization column
# Signals Par, Med, ParXX, Fdg become T in localization
# Signals Brain, t2Flair and t1ce become H in localization
# Signal portal becomes A in localization
# Signal FdgBody, TAP become TAP in localization
# If signal is empty then localization is also empty
# If other signals, we need to add condition otherwise localization is empty for those new signals

# localization T
Series_inventory.loc[Series_inventory['signal'].str.contains('Par') | Series_inventory['signal'].str.contains('Med') | Series_inventory['signal'].str.contains('Fdg') , 'localization'] = 'T'

# localization H
Series_inventory.loc[Series_inventory['signal'].str.contains('Brain') | Series_inventory['signal'].str.contains('t2flair') | Series_inventory['signal'].str.contains('t1ce') , 'localization'] = 'H'

# localization A
Series_inventory.loc[Series_inventory['signal']=='A', 'localization'] = 'A'
Series_inventory.loc[Series_inventory['signal']=='portal', 'localization'] = 'A'

# localization TAP
Series_inventory.loc[Series_inventory['signal'].str.contains('TAP') | Series_inventory['signal'].str.contains('FdgBody'), 'localization'] = 'TAP'

# localization AP
Series_inventory.loc[Series_inventory['signal'] =='AP', 'localization'] = 'AP'

# localization TA
Series_inventory.loc[Series_inventory['signal'] =='TA', 'localization'] = 'TA'



# Creation of 3D_reconstruction
# Not sure about the process so we create it empty for now
Series_inventory['3D_reconstruction']='' 

# We drop the column series_id_rad because we don't need it anymore 
Series_inventory = Series_inventory.drop(['series_id_rad'], axis=1)


# Creation of a new unique ID in the column series_id
# It starts at 0 and increments by 1 until the end of Series_inventory table size
Series_inventory.insert(0, 'series_id', range(0, 0 + len(Series_inventory)))


# Putting all columns of Series_inventory in the correct order
Series_inventory = Series_inventory[['series_id', 'prev_series_id', 'series_uid','project', 'pathology','cohort',
                                     'patient_ID', 'clinical_id', 'study_date','timepoint','modality','series_name',
                                     'localization','signal', 'filter_type', 'acquisition_axis', 'x_voxel_size',
                                     'y_voxel_size','z_voxel_size','3D_reconstruction', 'import_date', 'image_removal']] 
              



# Replace nan values in pathology by blank spaces (string)
# when we add new patients in radiomics DB, column pathology is nan for those new patients
# and the big loop to create target_ref_series doesn't work because pathology column not considered as string
# See if problems at first iteration when we don't have nan in pathology column
# Otherwise do a loop if nan then fill na else nothing
Series_inventory['pathology'] = Series_inventory['pathology'].fillna(value='')


# Transfer table Series_Inventory to InventoryDB version 0 or latest version + 1 depending on the value of conn_version
Series_inventory.to_sql('Series_inventory', conn_version)

    






#################################### IMPORT OF TABLES FROM RADIOMICS DB AND DEEP LEARNING TEAM

# Import of Model table 
# Later we'll probably do a command to open an existing file (csv or other)
# For now we create it manually and we store it in Model_rad
Model_rad = {'model_uid': [1, 2, 3, 4, 5, 6, 7, 8],
             
               'model_name': ['model_seg_nsclc_lung_hard', 'model_segml_nsclc_tum_hard','model_seg_nsclc_tum_hard-soft',
                              'model_seg_covid_lung_hard', 'model_seg_glioma_tum_t2flair','model_seg_meningioma_tum_t1ce',
                              'model_seg_gbm_tum_t1ce-t2flair', 'model_seg_breast-cancer_tum_fdg'],
               
               'pathology': ['nsclc', 'nsclc', 'nsclc', 'covid', 'glioma', 'meningioma', 'gbm', 'breast-cancer'],
               'purpose': ['', '', '', '', '', '', '', ''],
               
               'target': ['lung', 'tum', 'tum', 'lung', 'tum', 'tum', 'tum', 'tum'],
               
               'pathology_id': [1, 2, 2, '', 8, 9, 10, ''],
               
               'required_localization': ['T', 'T', 'T', 'T', 'H', 'H', 'H', 'B'],
               
               'multimodal': [0, 0, 1, 0, 0, 0, 1, 0],
               
               'ref_modality': ['CT', 'CT', 'CT', 'CT', 'MRI', 'MRI', 'MRI', 'PET'],
               
               'ref_signal_list': ['hard', 'hard', 'hard', 'hard', 't2flair', 't1ce', 't1ce', 'fdg'],
               
               'additional_modality': ['', '', 'CT', '', '', '', 'MRI', ''],   
               
               'additional_signal_list': ['', '', 'soft', '', '', '', 't2flair', ''],
               
               'version_availability': ['v0.0', '', 'v1.0', 'v1.0', 'v1.0', 'v1.0', '', '']
               }

# We create a Data Frame of Model_rad 
Model_rad= pd.DataFrame(Model_rad)

# Before creating target_ref_series tables we need to get unique values from target that are in Model_rad
# Creation of a list named target_unique containing all targets in Model_rad
target_unique = Model_rad['target'].unique().tolist() 



# Import of segmentation_base table from radiomics DB stored in Segmentation_rad
# Maybe we don't need to import lesion_id column ?
Segmentation_rad = pd.read_sql_query("SELECT id as seg_id, lesion as lesion_id, comment as ref_seg_name FROM segmentation_base", conn_rad)

# Import of morpho_features table from radiomics DB stored in Morpho_features_rad
Morpho_Features_rad = pd.read_sql_query("SELECT segmentation as seg_id, meshVolume as current_volume FROM morpho_features", conn_rad)

# Import seg_rigid_registration_series table from radiomics DB stored in seg_rigid_registration_series_rad
seg_rigid_registration_series_rad = pd.read_sql_query("SELECT segmentation as seg_id, series as series_id_rad FROM seg_rigid_registration_series", conn_rad)



################################################ MODIFICATION OF SEGMENTATION RAD 
# Where we have ref seg name we keep the ones that have good target and good signals 

# Creation of a list that contains all possible signals (to be completed if more possible signals)
signals_list = ['t1ce', 'portal', 'Par', 'Med', 't2flair']

# Creation of column target that contains the words before the first hyphen so that contains the correct target
Segmentation_rad["target"] = Segmentation_rad['ref_seg_name'].str.extract(r'([^-]+)')
# Creation of column signals that contains the words between target- and -Pred/PredCorr/FromSig
Segmentation_rad ['signals'] = Segmentation_rad ['ref_seg_name'].str.extract(r'-([^-]+)(?:-|$)')

# Where signals in column signals match signals in signals_list we keep ref_seg_name otherwise we let ref_seg_name empty
# In theory it's enough to do that because if signal isn't correct it's because we don't have target + '-'
# But we can still let the second condition if there are syntax errors in target words (like lun instead of lung)
# So if target in target column doesn't match target in target_unique list we let ref seg name empty, otherwise we keep ref seg name as it is
conditions_ref_seg_name = [(Segmentation_rad['signals'].isin(signals_list))]
results_ref_seg_name = [Segmentation_rad['ref_seg_name']]
Segmentation_rad['ref_seg_name'] = np.select(conditions_ref_seg_name, results_ref_seg_name, default='')

# Condition for targets 
conditions_target = [(Segmentation_rad['target'].isin(target_unique))]
results_target = [Segmentation_rad['ref_seg_name']]
Segmentation_rad['ref_seg_name'] = np.select(conditions_target, results_target, default='')

# We drop the columns target and signals because we don't need them anymore
Segmentation_rad = Segmentation_rad.drop(['target', 'signals'], axis=1)








################################################ CREATION OF REG_SEGMENTATION TABLE

# We merge Series_from_rad with Series_inventory to get series_id from Series_inventory (id that we've generated)
ref_segmentation = Series_from_rad[['series_id_rad', 'series_uid']].merge(Series_inventory[['series_id', 'series_uid']])

# We merge ref_segmentation with seg_rigid_registration_series_rad to get seg_id, series_id_rad
ref_segmentation = ref_segmentation[['series_id_rad', 'series_id']].merge(seg_rigid_registration_series_rad[['seg_id', 'series_id_rad']])

# We merge ref_segmentation with Segmentation_rad to get the final ref_segmentation table with series_id (generated in Series_inventory table), ref_seg_name, seg_id
ref_segmentation = ref_segmentation[['series_id', 'seg_id']].merge(Segmentation_rad[['seg_id', 'ref_seg_name']])

# We rename the column series_id by series_inventory_id 
ref_segmentation.columns = ['series_inventory_id', 'seg_id', 'ref_seg_name']

# We've already filtered ref seg name to have correct targets and correct signals
# So here we filter to keep only rows where we have PredCorr

ref_segmentation = ref_segmentation[ref_segmentation['ref_seg_name'].str.contains("PredCorr")]



#########################################################################################################



############################## CREATION OF TARGET REF SERIES TABLES FROM RADIOMICS DB

# In the same way as Series_from_rad we're creating target_ref_series_from_rad first 

## Creation of the columns ref_series_inventory_id, pathology_id, 
# additional_ref_series_inventory_id and ref_seg_series_id in target_ref_series_from_rad tables


# For each target in model rad that we stored in target_unique list
for target in target_unique: 
    # We create empty data frames target_ref_series_from_rad
    vars()[target+'_ref_series_from_rad'] = pd.DataFrame() 
    
    # For each patient
    for patient in Series_inventory.patient_ID.unique(): 
    
        # For each patients and their studies
        for study in Series_inventory[Series_inventory['patient_ID']==patient].study_date.unique():     
            print(patient)
            print(study)
            z=100 # We initialize z for ref series at 100
            z_additional = 100 # we initialize z_additional for additional ref series at 100
            ID_ref_series = -1 # We initialize ID_ref_series at -1
            ID_additional_ref_series = -1 # We initialize additional_ID_ref_series at -1
            pathology_id = -1 # We initialize pathology_ID at -1
            ref_segmentation_id = -1 # We initialize ref_segmentation_id at -1
            
            
            # For patient 1 and its same studies, then for patient 1 and its other same studies 
            # Then for patient 2 and its same studies, then for patient 2 and its other same studies
            # Etc
            for series in Series_inventory[(Series_inventory.patient_ID==patient) & (Series_inventory.study_date==study)]['series_id'].values:
                #print(series)
                

            # For each rows in pathology table
                for rows in range(pathology.shape[0]): 
            
                    # If a row in pathology matches all condition :
                    # same pathology, modality, signal, ref axis and common localization in tables Series_inventory and pathology
                    # and if target in pathology is the same than the target in target_unique list
                    # and if z_voxel_size of the corresponding series is inferior than z
                    if (Series_inventory.at[series, 'pathology']!='' 
                        and Series_inventory.at[series, 'pathology'] in pathology.at[rows,'pathology'] 
                        and Series_inventory.at[series, 'modality']!='' 
                        and Series_inventory.at[series, 'modality'] in pathology.at[rows,'ref_modality'] 
                        and Series_inventory.at[series, 'localization']!= '' 
                        and Series_inventory['localization'].str.contains(pathology['ref_loc'][rows])[series]
                        and Series_inventory.at[series, 'signal']!='' 
                        and Series_inventory.at[series, 'signal'] in pathology.at[rows, 'ref_signal']
                        and Series_inventory.at[series, 'acquisition_axis']!='' 
                        and Series_inventory.at[series, 'acquisition_axis'] in pathology.at[rows, 'ref_axis']
                        and pathology.at[rows, 'target']== target 
                        and Series_inventory.at[series, 'z_voxel_size']<z):
                            
       
                            # If all condition filled then z takes the value of z_voxel_size of the corresponding serie
                            z = Series_inventory.at[series, 'z_voxel_size']
                            # If all condition filled then ID_ref_series takes the series_id of the corresponding serie (series_id that we've generated in Series_inventory)
                            ID_ref_series = Series_inventory.at[series, 'series_id']
                            # If all condition filled then pathology_id takes the value of patho_id of the corresponding rows
                            pathology_id = pathology.at[rows, 'patho_id']
                            
    
         
                    
                    # If all conditions for ref series are filled then we want to know if all condition are filled for an additional ref series
                    # so if same pathology, additional modality, additional signal, ref axis and common localization in table Series_inventory and pathology
                    # and if target in pathology is the same than the target in target_unique list
                    # and if z_voxel_size of the corresponding series is inferior than z_additional
                    elif (Series_inventory.at[series, 'pathology']!='' 
                        and Series_inventory.at[series, 'pathology']  in pathology.at[rows,'pathology'] 
                        and Series_inventory.at[series, 'modality']!='' 
                        and Series_inventory.at[series, 'modality']  in pathology.at[rows,'additional_modality'] 
                        and Series_inventory.at[series, 'localization']!= '' 
                        and Series_inventory['localization'].str.contains(pathology['ref_loc'][rows])[series]
                        and Series_inventory.at[series, 'signal']!='' 
                        and Series_inventory.at[series, 'signal'] in pathology.at[rows, 'additional_signal']
                        and Series_inventory.at[series, 'acquisition_axis']!='' 
                        and Series_inventory.at[series, 'acquisition_axis'] in pathology.at[rows, 'ref_axis']
                        and pathology.at[rows, 'target']== target
                        and Series_inventory.at[series, 'z_voxel_size']<z_additional):
                            
                        
                            # If all condition are filled then z_additional takes the value of z_voxel_size of the corresponding additional serie
                            z_additional = Series_inventory.at[series, 'z_voxel_size']
                            # If all condition are filled then ID_additional_ref_series takes the value of series_id of the corresponding additional serie (series_id that we've generated in Series_inventory)
                            ID_additional_ref_series = Series_inventory.at[series, 'series_id']
                            # If all condition are filled then pathology_id takes the value of patho_id of the corresponding rows
                            pathology_id = pathology.at[rows, 'patho_id']
                            
                        
                    # If series are equal to the values in column series_inventory_id from ref_segmentation table
                    # and if ref_seg_name in ref_segmentation table contains target+'-'
                    if (ID_ref_series in ref_segmentation['series_inventory_id'].values) and (ref_segmentation['ref_seg_name'].str.contains(target+'-')).any():

                        
                        # If condition are filled then ref_segmentation_id takes the values of series
                        ref_segmentation_id = ID_ref_series

                       

            # If condition for ref series are filled and that ID_ref_series is superior than -1 
            # It means that the value superior than -1 in ID_ref_series is a series of reference
            if ID_ref_series > -1:
                # Then we fill the column ref_series_inventory_id in target_ref_series_from_rad with the values of ID_ref_series
                vars()[target+'_ref_series_from_rad'].at[series, 'ref_series_inventory_id'] = ID_ref_series
                
                # We fill the column pathology id in target_ref_series_from_rad with values of pathology_id from pathology table
                vars()[target+'_ref_series_from_rad'].at[series, 'pathology_id'] = pathology_id


                # If ID_additional_ref_series is superior than -1
                # It means that the value superior than -1 in ID_additional_ref_series is an additional series of reference
                if ID_additional_ref_series > -1:
                    # We fill the column additional_ref_series_inventory_id with the values of ID_additional_ref_series
                    vars()[target+'_ref_series_from_rad'].at[series, 'additional_ref_series_inventory_id'] = ID_additional_ref_series.astype(str)

            # If condition for segmentation ref series are filled and that ref_segmentation_id is superior than -1
            # It means that there is a serie that has a segmentation of reference
            if ref_segmentation_id > -1:
                # We fill the column ref_seg_series_id with the values of ref_segmentation_id in target_ref_series tables
                vars()[target+'_ref_series_from_rad'].at[series, 'ref_seg_series_id'] = ref_segmentation_id
                
                
                
            # If column additional_ref_series exists we do nothing  
            else: 
                if 'additional_ref_series_inventory_id' in vars()[target+'_ref_series_from_rad']:
                    vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad']
                    
                # If column additional_ref_series doesn't exist we create it empty because we need it to merge tables after  
                else: 
                    vars()[target+'_ref_series_from_rad']['additional_ref_series_inventory_id'] = ''
                
      
            # If target ref series have only one column (can only be additional_ref_series_inventory_id)
            # Then we create the other columns that we should have with the loop but we create them empty
            # So there won't be problems with merges after (if we don't have the columns the merges will throw an error)
            if len(vars()[target+'_ref_series_from_rad'].columns)==1:
                vars()[target+'_ref_series_from_rad'][['ref_series_inventory_id', 'pathology_id', 'ref_seg_series_id']]=''
            
      
        

 
########################## MERGES TO GET THE OTHER COLUMNS IN TARGET REF SERIES FROM RAD TABLES
########################## AND PROCESSING ON SOME COLUMNS OF TARGET REF SERIES FROM RAD TABLES

    # If there are nan in ref series inventory id we remove those rows in target ref series tables to keep rows only where we know ref series inventory id
    vars()[target+'_ref_series_from_rad']= vars()[target+'_ref_series_from_rad'][vars()[target+'_ref_series_from_rad']['ref_series_inventory_id'].notna()]
 

    # Merge target_ref_series_from_rad with Series_inventory to get series_uid
    vars()[target+'_ref_series_from_rad']= vars()[target+'_ref_series_from_rad'].merge(Series_inventory[['series_id','series_uid']], left_on='ref_series_inventory_id', right_on='series_id', how='left')

    
    # Merge with Series_rad to get series_id_rad
    vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad'][['ref_series_inventory_id', 'pathology_id', 'additional_ref_series_inventory_id', 'series_uid', 'ref_seg_series_id']].merge(Series_rad[['series_id_rad', 'series_uid']], how='left')
    
 
    # Merge target_ref_series_from_rad with seg_rigid_registration_series_rad to get the seg id
    vars()[target+'_ref_series_from_rad']= vars()[target+'_ref_series_from_rad'].merge(seg_rigid_registration_series_rad[['seg_id','series_id_rad']], how='left')

    
    # Merge target_ref_series_from_rad with Segmentation_rad to get ref_seg_name corresponding to ref_series_inventory_id
    vars()[target+'_ref_series_from_rad'] = pd.merge(vars()[target+'_ref_series_from_rad'],Segmentation_rad, how='left')
  

    # Drop columns that we don't need anymore : series_uid, series_id_rad (from radiomics DB), lesion_id 
    vars()[target+'_ref_series_from_rad']= vars()[target+'_ref_series_from_rad'].drop(['series_uid', 'series_id_rad', 'lesion_id'], axis=1)

    
    # We fill ref_seg_name by nan where ref_seg_name are empty because they are empty strings and not np.nan
    # If they are empty and not np.nan the command right after isn't working
    vars()[target+'_ref_series_from_rad'].loc[vars()[target+'_ref_series_from_rad'].ref_seg_name == '', 'ref_seg_name'] = np.nan
    
    
    # Where we have values in ref_seg_name we keep only the one that starts with target+'-' corresponding to target in target_ref_series tables
    vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad'][vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.contains(target+'-', na=True)]
    
               

                                        

    # We apply the function TargetRefLesionName and we groupby ref_series_inventory_id
    # Function explained in the file TargetRefLesionName
    # Used to keep one serie for each study with a priority order (PredCorr first, FromSig second, Pred last)
    
    # If target_ref_series is empty it equals to itself otherwise we apply the function TargetRefLesionName
    if vars()[target+'_ref_series_from_rad'].empty:
        vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad']
        
    else:
        vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad'].groupby("ref_series_inventory_id").apply(TargetRefLesionName).reset_index(drop=True) 
    
    
    
    # Creation of seg_import_on_ref_series column
    # if ref_series_seg_name is not PredCorr nor From 
    # and if ref_seg_series_id is not empty and if ref_seg_series_id is different than ref_series_inventory_id
    # and if ref_seg_series_registration_validity is equal to 1
    # Then we put 1 in seg_import_on_ref_series
    # Otherwise we put 0 in seg_import_on_ref_series
  
    # Creation of ref_seg_series_registration_validity 
    # We put 1 in ref_seg_registration_validity if ref_series_inventory_id and ref_seg_series_id are the same
    vars()[target+'_ref_series_from_rad']['ref_seg_series_registration_validity']=''
    vars()[target+'_ref_series_from_rad'].loc[vars()[target+'_ref_series_from_rad'].ref_series_inventory_id == vars()[target+'_ref_series_from_rad'].ref_seg_series_id, 'ref_seg_series_registration_validity'] = 1
    
   
  
    # Creation of seg_import_on_ref_series with condition explained above
    vars()[target+'_ref_series_from_rad']['seg_import_on_ref_series'] = np.where(~(vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.contains("PredCorr") | (vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.contains("From"))) & (vars()[target+'_ref_series_from_rad']['ref_seg_series_registration_validity']==1) & (vars()[target+'_ref_series_from_rad']['ref_seg_series_id']!= vars()[target+'_ref_series_from_rad']['ref_series_inventory_id']) & (vars()[target+'_ref_series_from_rad']['ref_seg_series_id'].notna()), 1, 0)
    
    
    # Creation of seg_import_on_additional_ref_series column
    # if ref_series_seg_name is PredCorr or From 
    # and if additional_ref_series_registration_validity is equal to 1
    # Then we put 1 in seg_import_on_additional_ref_series
    # Otherwise we put 0 in seg_import_on_additional_ref_series
    
    # Creating additional_ref_series_registration_validity with values of 1 inside to test
    # TO REMOVE LATER BECAUSE WE CAN CREATE THEM EMPTY DURING VERSIONING
    vars()[target+'_ref_series_from_rad']['additional_ref_series_registration_validity'] = 1
    
    # Creation of seg_import_on_additional_ref_series with condition explained above
    vars()[target+'_ref_series_from_rad']['seg_import_on_additional_ref_series'] = np.where((vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.contains("PredCorr") | vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.contains("From")) & (vars()[target+'_ref_series_from_rad']['additional_ref_series_registration_validity']==1) , 1, 0)
                                                
           
                                                
           
    # Creation of ref_series_processing 
    # If seg_import_on_ref_series == 1, we put 'import' in ref_series_processing 
    # If seg_import_on_ref_series == 0 and ref_seg_name is empty, we put 'pred' in ref_series_processing
    # If seg_import_on_ref_series == 0 and ref_seg_name ends with 'Pred', we put 'correction' in ref_series_processing
    # Else, we put 'learning' in ref_series_processing
    
    # Conditions explained above
    conditions = [(vars()[target+'_ref_series_from_rad']['seg_import_on_ref_series']==1),
                  (vars()[target+'_ref_series_from_rad']['seg_import_on_ref_series']==0) & (pd.isnull(vars()[target+'_ref_series_from_rad']['ref_seg_name'])),
                  (vars()[target+'_ref_series_from_rad']['seg_import_on_ref_series']==0) & (vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.endswith("Pred", na=True))]
    
    # Result expected for each condition (result for condition 1, result for condition 2, result for condition 3)
    results = ['import', 'pred', 'correction']
    
    # Creation of ref_series_processing based on those conditions
    vars()[target+'_ref_series_from_rad']['ref_series_processing'] = np.select(conditions, results, default = 'learning')
    
    
    
    # Creation of additional_ref_series_processing  
    # If seg_import_on_additional_ref_series == 1 we put 'import' in additional_ref_series_processing
    # Else, additional_ref_series_processing is empty 
    vars()[target+'_ref_series_from_rad']['additional_ref_series_processing'] = np.where(vars()[target+'_ref_series_from_rad']['seg_import_on_additional_ref_series']==1, 'import', '')
    
    
    # Merge target_ref_series_from_rad with Morpho_Features_rad to get current_volume
    vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad'].merge(Morpho_Features_rad[['seg_id','current_volume']], on = 'seg_id', how='left') 
    
  
    # We keep the current volume only where we have PredCorr in ref_seg_name cols 
    vars()[target+'_ref_series_from_rad']['current_volume'] = vars()[target+'_ref_series_from_rad']['current_volume'][vars()[target+'_ref_series_from_rad']['ref_seg_name'].str.contains("(Pred)\w+", na=False)]        
    
        
    # Keep rows as nan in lung current volume where we don't know the lesion_name and put 0 in the other nan values where we have lesion name but not the value of the lung current volume
    vars()[target+'_ref_series_from_rad']['current_volume'] = np.where(pd.notna(vars()[target+'_ref_series_from_rad']['ref_seg_name']) & pd.isnull(vars()[target+'_ref_series_from_rad']['current_volume']), 0, vars()[target+'_ref_series_from_rad']['current_volume'])
    
    # Drop seg_id column because we don't need it anymore 
    vars()[target+'_ref_series_from_rad'] = vars()[target+'_ref_series_from_rad'].drop(['seg_id'], axis=1)
    
    

 
##################################### CREATION OF TARGET_REF_SERIES TABLES IF INVENTORY_DB EXISTS OR NOT  


# If an Inventory DB exist we connect to it (the latest) thanks to the variable previous_version
# We store this connection in the variable conn_previous
if version > 0:
    conn_previous =  sqlite3.connect("InventoryDB_v{}.db".format(previous_version))
    
    # For each target in target_unique list
    for target in target_unique: 
        
        # We create target_ref_series_from_prev with data from target_ref_series in the latest Inventory_DB (we import current volume as previous volume so that we can put it as previous volume in the new/current target_ref_series table)
        vars()[target+'_ref_series_from_prev'] = pd.read_sql_query("SELECT ref_series_inventory_id, additional_ref_series_registration_validity, ref_seg_series_registration_validity, current_volume as previous_volume, seg_quality, target_complexity FROM {}_ref_series".format(target), conn_previous)
        
        
     
       
        # We create the new current target_ref_seg tables by merging target ref seg from prev with target ref seg from rad 
        # It allows to have the data from previous version (manual) columns :
        # (previous volume (which is the current volume in target ref seg from prev), seg_quality,
        # target_complexity, and additional_ref_series_registration_validity
        vars()[target+'_ref_series'] = vars()[target+'_ref_series_from_rad'][['ref_series_inventory_id',
                                                                              'pathology_id', 
                                                                              'additional_ref_series_inventory_id',
                                                                              'ref_seg_series_id',
                                                                              'ref_seg_name',
                                                                              'ref_seg_series_registration_validity',
                                                                              'seg_import_on_ref_series',
                                                                              'seg_import_on_additional_ref_series',
                                                                              'ref_series_processing',
                                                                              'additional_ref_series_processing',
                                                                              'current_volume']].merge(vars()[target+'_ref_series_from_prev'][['ref_series_inventory_id', 'additional_ref_series_registration_validity',
                                                                                                                                                         'previous_volume','seg_quality',
                                                                                                                                                         'target_complexity']], how='left').drop_duplicates()
                                                                                                                                                      
                                                                                                                                                       
        # When we import the current_volume as previous_volume from target_ref_series_from_prev and we put it in the column previous_volume in target_ref_series tables, 
        # If there isn't any match there are NAN in previous_volume so we can fill with 0 where we know the current_volume and the ref_seg_name and where previous volume is nan 
        # Otherwise we keep the values that are in previous_volume if there are any values                                                                                                                                               
        vars()[target+'_ref_series']['previous_volume'] = np.where(pd.notna(vars()[target+'_ref_series']['ref_seg_name']) & pd.notna(vars()[target+'_ref_series']['current_volume']) & (vars()[target+'_ref_series']['previous_volume'].isna()), 0, vars()[target+'_ref_series']['previous_volume'])
      
        
# If Inventory DB doesn't exist we create target_ref_series tables equals to ref_series_from_rad tables
else:
    # For each target in target_unique list
    for target in target_unique: 
        vars()[target+'_ref_series'] = vars()[target+'_ref_series_from_rad'].copy()
       
        # When a previous table doesn't exist we fill previous_volume column with 0 where we have values in current volume and we let blank spaces in previous_volume where there are nan in ref_seg_name corresponding rows
        vars()[target+'_ref_series']['previous_volume'] = '' # Creating the col previous_volume empty of data
        vars()[target+'_ref_series']['previous_volume'] = np.where(pd.notna(vars()[target+'_ref_series']['ref_seg_name']) & pd.notna(vars()[target+'_ref_series']['current_volume']), 0, vars()[target+'_ref_series']['previous_volume']) 
       
        
        # We convert previous_volume data type from object to float 
        #vars()[target+'_ref_series']['previous_volume'] = vars()[target+'_ref_series']['previous_volume'].astype(float)
        
       
        # We add empty columns (manual columns) to the first target_ref_series tables when there isn't any InventoryDB created yet
        # ADD THE COLUMNS 'additional_ref_series_registration_validity'
        # FOR NOW I CREATE THEM BEFORE TO TEST THE PROCESSINGS
        vars()[target+'_ref_series'][['seg_quality','target_complexity']]= ''
        
        #### MAYBE CREATE PREV SERIES ID like we did for Series_inventory table ? 
      
    

    





######################## PROCESSING ON TARGET_REF_SERIES TABLES IF INVENTORY_DB EXIST OR NOT

for target in target_unique:
    
    # If target ref series tables are empty we juste add the column seg_correction empty
    if vars()[target+'_ref_series'].empty:
        
        # CHECK IF THERE ARE COLS TO ADD EMPTY
        vars()[target+'_ref_series'][['ref_series_id', 'change_status']] = ''
        
        # Putting columns in the correct order 
        vars()[target+'_ref_series'] = vars()[target+'_ref_series'][['ref_series_id', 'pathology_id', 'ref_series_inventory_id',
                                                                     'additional_ref_series_inventory_id', 'ref_seg_series_id', 'additional_ref_series_registration_validity',
                                                                     'ref_seg_series_registration_validity', 'ref_seg_name', 'previous_volume',
                                                                     'current_volume','change_status', 'seg_quality',
                                                                     'target_complexity', 'seg_import_on_ref_series', 'seg_import_on_additional_ref_series',
                                                                     'ref_series_processing', 'additional_ref_series_processing']]
        
        # Rename the column ref_seg_name by ref_series_seg_name in target_ref_series tables
        vars()[target+'_ref_series'] = vars()[target+'_ref_series'].rename(columns={"ref_seg_name": "ref_series_seg_name"})
        
        # Transfering target_ref_series tables into the new version of InventoryDB        
        vars()[target+'_ref_series'].to_sql(target+'_ref_series', conn_version)
        
    else:
        
        ### Creation of change status column
        # If previous_volume == current_volume then put 0 in change status
        vars()[target+'_ref_series'].loc[vars()[target+'_ref_series']['previous_volume'] == vars()[target+'_ref_series']['current_volume'], 'change_status'] = 0
        
    
        # If previous_volume is different than current_volume then put the difference between current and previous volume in change_status
        vars()[target+'_ref_series'].loc[vars()[target+'_ref_series']['previous_volume'] != vars()[target+'_ref_series']['current_volume'], 'change_status'] = vars()[target+'_ref_series']['current_volume'] - vars()[target+'_ref_series']['previous_volume']
       

        # Generating a unique ID for target_ref_series_tables to make the link between all tables of InventoryDB
        # It starts at 0 and increments by 1 until the end of target_ref_series_tables length
        vars()[target+'_ref_series'].insert(0, 'ref_series_id', range(0, 0 + len(vars()[target+'_ref_series'])))
       
        # Putting columns in the correct order      
        vars()[target+'_ref_series'] = vars()[target+'_ref_series'][['ref_series_id', 'pathology_id', 'ref_series_inventory_id',
                                                                     'additional_ref_series_inventory_id', 'ref_seg_series_id', 'additional_ref_series_registration_validity',
                                                                     'ref_seg_series_registration_validity', 'ref_seg_name', 'previous_volume',
                                                                     'current_volume','change_status', 'seg_quality',
                                                                     'target_complexity', 'seg_import_on_ref_series', 'seg_import_on_additional_ref_series',
                                                                     'ref_series_processing', 'additional_ref_series_processing']]
        
        
        
        
        # Rename the column ref_seg_name by ref_series_seg_name in target_ref_series tables
        vars()[target+'_ref_series'] = vars()[target+'_ref_series'].rename(columns={"ref_seg_name": "ref_series_seg_name"})
       
        # Transfering target_ref_series tables into the new version of InventoryDB        
        vars()[target+'_ref_series'].to_sql(target+'_ref_series', conn_version)
      
           








####################################### CREATION OF PREDICTION TABLE 

# Fill nan values of target ref series tables by blank spaces otherwise there is an error for prediction table at signal_additional = Series_inventory.signal[float(additional_ref_series_inventory_id)]
for target in target_unique:
    vars()[target+'_ref_series']['additional_ref_series_inventory_id'] = vars()[target+'_ref_series']['additional_ref_series_inventory_id'].fillna('')
    


# Creating table prediction empty
prediction = pd.DataFrame()
                

############################################

# For each indexes and models in Model_rad table (models = rows in Model_rad table)
for index,models in Model_rad.iterrows():
    
    # If models are available (if version_availability not empty)
    if models['version_availability']!='':

        # We define the variable target_available (targets where models are available)
        target_available = models.target

        # We define the variable pathology_id (pathologies id where models are available)
        pathology_id = models.pathology_id

        # We define the variable model_uid (models uid where models are available)
        model_uid = models.model_uid

        # We define the variable is_monomodal (monomodal models (0) or multimodal models (1) where models are available)
        is_monomodal = models.multimodal
       

############################################

        # For indexes_target_series and series_target in target_available_ref_series_from_rad (series_target = rows in target available ref series from rad)
        for index_target_series, series_target in vars()[target_available+'_ref_series'].iterrows():
            
     
            # If  model is monomodal (so if is_monomodal equals to 0)
            if is_monomodal==0:
                
                # If seg processing column in target ref series tables is equal to pred and if pathology_id column in target ref series tables is equal to pathology_id variable (in Model_rad table)
                if series_target.ref_series_processing=="pred" and series_target.pathology_id == pathology_id:

                    # We define the variable ref_series_id (ref series id generated in target_ref_series_from_rad tables)
                    ref_series_id = series_target.ref_series_id
                    # We define the variable ref_series_inventory_id equals to the column ref series inventory id in target available ref series from rad tables
                    ref_series_inventory_id = series_target.ref_series_inventory_id
                   
                    # We define the variable signal equals to signals in the column signal in Series inventory table that match the ref series inventory id selected
                    signal = Series_inventory.signal[ref_series_inventory_id]
                           
                    # We define the variable output_name that contains string MASK_ + variable target available + string '_' + variable signal + string '_Pred'
                    output_name = 'MASK_'+ target_available + '_' + signal + '_Pred' 
                    # We define a variable that contains the rows of prediction table with the 4 columns model_uid, target, target_ref_series_id, output_name
                    new_row = {'model_id': model_uid,'target': target_available, 'target_ref_series_id': ref_series_id, 'output_name': output_name}
                    # We add the rows to prediction table
                    prediction = prediction.append(new_row, ignore_index=True)
     
                
            #Else, if  model is multimodal (so if is_monomodal equals to 1)
            elif is_monomodal==1:
                
                # We define the variable additional_ref_series_inventory_id that contains the values of the column additional ref series inventory id from target ref series tables
                additional_ref_series_inventory_id = series_target.additional_ref_series_inventory_id
                #print(additional_ref_series_inventory_id)
                
                # If this variable additional_ref_series_inventory_id is different than blank space and if seg processing column in target ref series tables is equals to pred and if pathology_id column in target ref series tables is equal to pathology_id variable (in Model_rad table)
                if (additional_ref_series_inventory_id!='') and (series_target.ref_series_processing=="pred") and (series_target.pathology_id == pathology_id):
                    
                    
                    print(additional_ref_series_inventory_id)
                    # We define the variable ref_series_id (ref series id generated in target_ref_series_from_rad tables)
                    ref_series_id = series_target.ref_series_id
                    # We define the variable ref_series_inventory_id equals to the column ref series inventory id in target available ref series from rad tables
                    ref_series_inventory_id = series_target.ref_series_inventory_id
                   
                    # We define the variable signal_ref equals to signals in the column signal in Series inventory table that match the ref series inventory id selected
                    signal_ref = Series_inventory.signal[ref_series_inventory_id]
                    
                    
                    # Here bug because nan in addition aref series inventory id i think
                    # We define the variable signal_additional equals to signals in the column signal in Series inventory table that match the additional ref series inventory id selected
                    signal_additional = Series_inventory.signal[float(additional_ref_series_inventory_id)]
                    
                    # We define the variable output_name that contains string MASK_ + variable target available + string '_' + variable signal_ref + string'-' + variable signal_additional + string '_Pred'
                    output_name = 'MASK_'+ target_available + '_' + signal_ref + '-' + signal_additional + '_Pred' 
                    # We define a variable that contains the rows of prediction table with the 4 columns model_uid, target, target_ref_series_id, output_name
                    new_row = {'model_id': model_uid,'target': target_available,'target_ref_series_id': ref_series_id, 'output_name': output_name}
                    # We add the rows to prediction table
                    prediction = prediction.append(new_row, ignore_index=True)




# Adding a prediction_id in prediction table
prediction.insert(0, 'prediction_id', range(0, 0 + len(prediction)))
# Adding the column prediction_date empty in prediction table
prediction['prediction_date'] = '' 

# If table prediction is empty because no ref series matching condition, we let prediction = prediction empty with 2 columns prediction_id and prediction date
# then we add the other columns missing empty
if prediction.empty:
    prediction[['model_id', 'target', 'target_ref_series_id', 'output_name']]=''
    
    ## Exporting prediction table empty to a JSON file (usefull ? ?) QUESTION 
    
    #prediction.to_json(path_or_buf = '/Users/estoupmarion/prediction_json') 
    
    # If prediction is not empty, we put all columns with data in the correct order and we export prediction to a JSON file
else:
    
    # Putting the columns in the correct order
    prediction = prediction[['prediction_id', 'model_id', 'target', 'target_ref_series_id', 'output_name', 'prediction_date']]


    ## Exporting prediction table to a JSON file 
    
    prediction.to_json(path_or_buf = '/Users/estoupmarion/prediction_json')             
     








####################################### CREATION OF LEARNING TABLE 


# Creating table learning empty
learning = pd.DataFrame()
############################################

# For each indexes and models in Model_rad table (models = rows in Model_rad table)
for index,models in Model_rad.iterrows():
    # We define the variable target_available (all targets where models are available and where models are not available)
    target_available = models.target

    # We define the variable pathology_id (pathologies id where models are available and where models are not available)
    pathology_id = models.pathology_id

    # We define the variable model_uid (models uid where models are available and where models are not available)
    model_uid = models.model_uid

    # We define the variable is_monomodal (monomodal models (0) or multimodal models (1) where models are available and where models are not available)
    is_monomodal = models.multimodal

    ############################################

    # For indexes_target_series and series_target in target_available_ref_series_from_rad (series_target = rows in target available ref series from rad)
    for index_target_series, series_target in vars()[target_available+'_ref_series'].iterrows():
        
        
         # If  model is monomodal (so if is_monomodal equals to 0)
        if is_monomodal==0:
            # If seg processing column in target ref series tables is equal to learning  and if pathology_id column in target ref series tables is equal to pathology_id variable (in Model_rad table)
            if series_target.ref_series_processing=="learning" and series_target.pathology_id == pathology_id:
                print(series_target)
                # We define the variable ref_series_id (ref series id generated in target_ref_series_from_rad tables)
                ref_series_id = series_target.ref_series_id
        
        ########################################################################################
                # If model is available (if version_availability not empty)
                if models['version_availability'] != '':
                    
                    # Model_version is equal to values in version_availability in Model_rad table
                    model_version = models['version_availability']
                    # Model_version is equal to v(x+1).y (ex : v12.0 -> v13.0 or v13.1 -> v14.1 etc)
                    # string v + x+1 + '.' + last character of the whole string
                    model_version = 'v' + str(int(re.search(r'\d+', model_version).group(0)) + 1) + '.' + model_version[-1]
         
                    # We define a variable that contains the rows of learning table with the 4 columns model_uid, target, ref_series_id, model_version
                    new_row = {'model_id': model_uid,'target': target_available, 'ref_series_id': ref_series_id, 'model_version': model_version}
                    # We add the rows to learning table
                    learning = learning.append(new_row, ignore_index=True)
                    
         ########################################################################################               
                else:
                    # We define the variable first_model_version as 'v0.0' for all model_version that don't have an available model yet
                    first_model_version = 'v0.0'
                  
        
                    # We define a variable that contains the rows of learning table with the 4 columns model_uid, target, target_ref_series_id, model_version
                    new_row = {'model_id': model_uid,'target': target_available, 'ref_series_id': ref_series_id, 'model_version': first_model_version}
                    # We add the rows to learning table
                    learning = learning.append(new_row, ignore_index=True)
        
         ######################################################################################## 

        if is_monomodal == 1:
            # If seg processing column in target ref series tables is equal to learning  and if pathology_id column in target ref series tables is equal to pathology_id variable (in Model_rad table)
            if series_target.additional_ref_series_inventory_id!='' and series_target.ref_series_processing=="learning" and series_target.pathology_id == pathology_id:
                
                # We define the variable ref_series_id (ref series id generated in target_ref_series_from_rad tables)
                ref_series_id = series_target.ref_series_id
                print(ref_series_id)
        
        ########################################################################################
                # If model is available (if version_availability not empty)
                if models['version_availability'] != '':
                    
                    # Model_version is equal to values in version_availability in Model_rad table
                    model_version = models['version_availability']
                    # Model_version is equal to v(x+1).y (ex : v12.0 -> v13.0 or v13.1 -> v14.1 etc)
                    # string v + x+1 + '.' + last character of the whole string
                    model_version = 'v' + str(int(re.search(r'\d+', model_version).group(0)) + 1) + '.' + model_version[-1]
         
                    # We define a variable that contains the rows of learning table with the 4 columns model_uid, target, ref_series_id, model_version
                    new_row = {'model_id': model_uid,'target': target_available, 'ref_series_id': ref_series_id, 'model_version': model_version}
                    # We add the rows to learning table
                    learning = learning.append(new_row, ignore_index=True)
                    
         ########################################################################################               
                else:
                    # We define the variable first_model_version as 'v0.0' for all model_version that don't have an available model yet
                    first_model_version = 'v0.0'
                  
        
                    # We define a variable that contains the rows of learning table with the 4 columns model_uid, target, target_ref_series_id, model_version
                    new_row = {'model_id': model_uid,'target': target_available, 'ref_series_id': ref_series_id, 'model_version': first_model_version}
                    # We add the rows to learning table
                    learning = learning.append(new_row, ignore_index=True)
                


 # Adding a prediction_id in prediction table
learning.insert(0, 'learning_id', range(0, 0 + len(learning)))
# Adding empty columns to learning table : weight, learning_date and learning_db_version
learning[['weight', 'learning_date', 'learning_db_version']] = ''

# If learning table is empty we let learning = learning with columns learning_id, weight, learning_date, learning_db_version empty
# And we add missing columns model_id, target, model_version, ref_series_id but empty
# We can export file to Inventory DB or JSON file, let's see ? ? 
if learning.empty:
    learning[['model_id', 'target', 'model_version', 'ref_series_id']]=''
    
    # If learning table is not empty we put all columns in the correct order and transfer the table in InventoryDB_vX
else:
    # Putting the columns in the correct order 
    learning = learning[['learning_id', 'model_id', 'target', 'model_version', 'ref_series_id', 'weight', 'learning_date', 'learning_db_version']]
    
    ## Exporting prediction table to a JSON file 
    learning.to_json(path_or_buf = '/Users/estoupmarion/learning_json')  


    


####################################### CREATION OF PROCESSING TABLE
     
# Create processing as an empty data frame
processing = pd.DataFrame()

# For index and rows in Model_rad table
for index,models in Model_rad.iterrows():
    # We define the variable target_available (all targets where models are available and where models are not available)
    target_available = models.target
   
    # For indexes_target_series and series_target in target_available_ref_series_from_rad (series_target = rows in target available ref series from rad)
    for index_target_series, series_target in vars()[target_available+'_ref_series'].iterrows():

        # If ref_series_processing is "correction" or "import"
        if series_target.ref_series_processing=="correction" or series_target.ref_series_processing=="import" or series_target.additional_ref_series_processing=='import':
            # We store the values of target_ref_series columns in variables
            
            ref_series_inventory_id = series_target.ref_series_inventory_id
            additional_ref_series_inventory_id = series_target.additional_ref_series_inventory_id
            ref_series_processing = series_target.ref_series_processing
            additional_ref_series_processing = series_target.additional_ref_series_processing
            ref_seg_name = series_target.ref_series_seg_name
            
            # We create a new row in processing table that contains the values of all variables that we've created
            new_row = {
                       'ref_series_inventory_id': ref_series_inventory_id,
                       'additional_ref_series_inventory_id': additional_ref_series_inventory_id,
                       'ref_series_processing': ref_series_processing,
                       'additional_ref_series_processing': additional_ref_series_processing,
                       'ref_series_ref_seg_name' : ref_seg_name
                       }
            # We add the rows to processing table
            processing = processing.append(new_row, ignore_index=True)
            # We remove duplicates if there are some
            processing = processing.drop_duplicates()
            
            
            
            
            
            
# If processing is empty we let it empty (or we can add empty columns likewise learning and prediction)          
if processing.empty:
    processing = processing
    # If processing is not empty
else:
    
    
    # Merge processing with Series_inventory on ref_series_inventory_id to get data from this table corresponding to ref series (patient_ID, study_date, series_name, image_removal)
    processing = processing.merge(Series_inventory[['series_id',
                                                    'patient_ID',
                                                    'series_name',
                                                    'study_date',
                                                    'image_removal']], left_on = 'ref_series_inventory_id', right_on = 'series_id')
    
    
    
    # Rename columns series_name by ref_series_description , patient_ID by patient_name and image_removal by ref_series_image_removal
    processing = processing.rename(columns={'series_name': 'ref_series_description', 'patient_ID': 'patient_name', 'image_removal': 'ref_series_image_removal'})

    # Convert the column additional_ref_series_inventory_id from object to integer
    processing['additional_ref_series_inventory_id'] = pd.to_numeric(processing['additional_ref_series_inventory_id'])
    
    # Merge processing with Series_inventory on additional_ref_series_inventory_id to get data from this table corresponding to additional ref series
    processing = processing.merge(Series_inventory[['series_id', 'series_name', 'image_removal']], left_on = 'additional_ref_series_inventory_id', right_on = 'series_id', how='left')

    # If 1 in image removal (corresponding to additional ref series) we put 'removal' in additional_ref_series_processing
    # If 1 in ref_series_image_removal (corresponding to ref series) we put 'removal' in ref_series_processing 
    
    processing.loc[processing.image_removal== '1', 'additional_ref_series_processing'] = 'removal'
    processing.loc[processing.ref_series_image_removal== '1', 'ref_series_processing'] = 'removal'
      
    
    # Rename series_name by additional_ref_series_description  
    processing = processing.rename(columns={'series_name' : 'additional_ref_series_description'})
    
    
    # When we import additional_ref_series we have the data in ref_series_processing for the corresponding ref series
    # So where additional_ref_series_processing is not null we let empty string in ref_series_processing
    processing['ref_series_processing'] = np.where(processing['additional_ref_series_processing']!='', '', processing['ref_series_processing']) 
  
    
    
    # I drop the columns that we don't need anymore 
    processing = processing.drop(['additional_ref_series_inventory_id', 'ref_series_inventory_id', 'series_id_x',
                        'ref_series_image_removal', 'series_id_y', 'image_removal'], axis=1)
    
    # put correct order for columns
    processing = processing[['patient_name', 'study_date', 'ref_series_description', 'ref_series_processing',
                   'additional_ref_series_description', 'additional_ref_series_processing']]


    
    
    # Transfer table processing to InventoryDB version 0 or latest version + 1 depending on the value of conn_version
    processing.to_sql('processing', conn_version)

    
    
    
    
    
 
    










 
  