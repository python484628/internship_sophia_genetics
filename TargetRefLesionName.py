#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 18:50:59 2021

@author: estoupmarion
"""

## Creation of Function TargetRefLesionName to chose ref seg name depending on priorities 
# If PredCorr in a study we return the row that contains PredCorr
# If not PredCorr but From in a study we return the row that contains From
# If not PredCorr nor From in a study we return the row that contains Pred
# Maybe add the last possibility ? If ref_seg_name is empty we return the row that is empty ?

def TargetRefLesionName(row):
    if row.ref_seg_name.str.contains("PredCorr", na=True).any():
        return row[row.ref_seg_name.str.contains("PredCorr", na=True)]
    
    elif row.ref_seg_name.str.contains("From",na=True).any(): 
        return row[row.ref_seg_name.str.contains("From", na=True)] 
    
    elif row.ref_seg_name.str.endswith("Pred", na=True).any():
        return row[row.ref_seg_name.str.endswith("Pred", na=True)] 
    
     #elif (row.ref_seg_name=='').any():
 #         return row[row.ref_seg_name=='']